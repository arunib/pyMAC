#!/usr/bin/python3.2 -tt
# -*- coding: UTF-8 -*-
""" CSMA MAC Protocols """

__version__ = "$Revision: 1.11 $"
# $Source$

__author__ = "Arun I B <arunib@smail.iitm.ac.in>"
__copyright__ = "Copyright (C) 2012 Arun I B"
#__license__     = TBD

from SimPy.Simulation import (SimEvent, Process,
                               waitevent, hold, request, release)
import random


INFINITY = 10000000


class TimeOut(SimEvent):
    """Time out event"""

    def __init__(self, delay, sim):
        """
        @delay  : Timeout delay
        @sim    : Simulation instance
        """
        SimEvent.__init__(self, name="TimeOut:" + str(delay), sim=sim)

        class _TimeOutProc(Process):
            """time out  process"""
            def timeout_delay(self, delay, event):
                """ wait for delay """
                yield hold, self, delay
                event.signal()

        temp = _TimeOutProc(name="_TimeOutProc:" + str(delay), sim=self.sim)
        self.sim.activate(temp, temp.timeout_delay(delay, self))


class dcf(Process):
    """
    The basic distributed coordination function.
    Define methods IdleDIFS, IdleSlot and Transmit
    """
    #def __init__(self):
    #    Object.__init__(self)
    #    pass
    def __init__(self, node, macparams):
        """
        MAC constructor
        @node     : Node
        @cwmin    : Minimum contention window
        @m        : Maximum #of back off stages
        @rlimit   : Retry Limit
        """
        Process.__init__(self, name="dcf" + str(node.id), sim=node.sim)
        self.node = node
        self.cwmin = macparams["cwmin"]
        self.rmax = macparams["rlimit"]
        self.m = macparams["m"]
        self.cwmax = (2 ** self.m) * self.cwmin  # Max Contention window size
        self.macdelays = []

    class _IdleDIFS(SimEvent):
        """
        IdleDIFS event
        Signals when the channel is Idle for a duration of DIFS.
        Channel Idle - > No transmissions
        """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, name="_IdleDIFS" + str(node.id),
                              sim=node.channel.sim)

            class __IdleDIFSProc(Process):
                """ idle difs event process """
                def idledifs_delay(self, event):
                    """ wait for delay """
                    while True:
                        # Wait for the channel to be idle
                        while(node.channel.ntx != 0):
                            yield (waitevent, self,
                            node.channel.uplink.Ch_Idle_Event0)
                        assert(node.channel.ntx == 0)
                        TO = TimeOut(delay=node.channel.difs, sim=self.sim)
                        yield (waitevent, self,
                               (node.channel.uplink.Tx_Start_Event,
                                node.channel.uplink.Tx_End_Event, TO))
                        assert(len(self.eventsFired) == 1)
                        if self.eventsFired[0] == TO:
                            """ If we assume that no packet transmission can
                            be completed within DIFS duration, its enough
                            to check channel.ntx at the end of DIFS event.
                            """
                            if(node.channel.ntx == 0):
                                assert(not
                                node.channel.uplink.Tx_End_Event.occurred)
                                event.signal()
                                break  # return
                        else:
                            """ If a packet transmission, starts or ends
                            restart the waiting process(for DIFS duration)
                            """
                            pass
            temp = __IdleDIFSProc(name="__IdleDIFSProc" + str(node.id),
                                  sim=node.channel.sim)
            self.sim.activate(temp, temp.idledifs_delay(self))

    class _IdleSlot(SimEvent):
        """ Signals when the channel is Idle for a slot duration.
            Channel Idle - > No transmissions """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, sim=node.channel.sim)

            class __IdleSlotProc(Process):
                """ idle slot event process """
                def idleslot_delay(self, event):
                    """ wait for delay """
                    channel = node.channel
                    while True:
                        while(channel.ntx != 0):
                            yield (waitevent, self,
                            channel.uplink.Ch_Idle_Event0)
                        assert(node.channel.ntx == 0)
                        TO = TimeOut(delay=channel.slot, sim=self.sim)
                        yield waitevent, self, (TO,
                        channel.uplink.Tx_Start_Event,
                        channel.uplink.Tx_End_Event)
                        assert(len(self.eventsFired) == 1)
                        if self.eventsFired[0] == TO:
                            """ If we assume that no packet transmission can
                            be completed within a slot duration, its enough
                            to check channel.ntx at the end of idleslot event.
                            """
                            if(channel.ntx == 0):
                                assert(not
                                channel.uplink.Tx_End_Event.occurred)
                                event.signal()
                                break
                        else:
                            """ If a packet transmission, starts or ends
                            restart the waiting process(for DIFS duration)
                            """
                            pass

            temp = __IdleSlotProc(name="__IdleSlotProc" + str(node.id),
                                  sim=node.channel.sim)
            self.sim.activate(temp, temp.idleslot_delay(self))

    class _Transmit(SimEvent, Process):
        """ Transmit a packet """

        def __init__(self, node):
            """ setup as event and process """
            SimEvent.__init__(self, name="__Transmit" + str(node.id),
                              sim=node.sim)
            Process.__init__(self, name="__Transmit" + str(node.id),
                             sim=node.sim)
            self.sim.activate(self, self.__transmit(self, node))

        def __transmit(self, event, node):
            """ transmit a packet"""
            yield request, self, node.channel.mutex, 1
            yield request, self, node.channel.subchannel
            node.channel.uplink.Tx_Start_Event.signal()
            yield release, self, node.channel.mutex, 1
            # Check for collision
            if (node.channel.ntx > node.channel.K):
                yield hold, self, node.pkttxtime
                yield request, self, node.channel.mutex, 2
                yield release, self, node.channel.subchannel
                node.channel.uplink.Tx_End_Event.signal()
                yield release, self, node.channel.mutex, 2
                node.pktcollided = node.pktcollided + 1
                # For collision, do not decrement packttosend
                # node.pkttosend = node.pkttosend - 1
                node.pktsent = node.pktsent + 1
                event.signal(False)
                return
            TO = TimeOut(node.pkttxtime, self.sim)
            Collided = False
            while True:
                # See if any other node transmits during the time. If the
                # number of transmissions is larger than K, all packets should
                # be marked as collided
                yield waitevent, self, (node.channel.uplink.Tx_Start_Event, TO)
                if(self.eventsFired[0] == TO):
                    if not Collided:
                        node.pktsucc = node.pktsucc + 1
                        success = True
                    else:
                        # at the end of tx time, if it has collided
                        node.pktcollided = node.pktcollided + 1
                        success = False
                    break
                else:
                    if (node.channel.ntx > node.channel.K):
                        Collided = True
                    # continue transmission
            yield request, self, node.channel.mutex, 2
            yield release, self, node.channel.subchannel
            node.channel.uplink.Tx_End_Event.signal()
            yield release, self, node.channel.mutex, 2
            if(success):
                node.pkttosend = node.pkttosend - 1
            node.pktsent = node.pktsent + 1
            event.signal(success)


class mprdcf(Process):
    """
    The basic distributed coordination function.
    Define methods IdleDIFS, IdleSlot and Transmit
    """
    #def __init__(self):
    #    Object.__init__(self)
    #    pass
    def __init__(self, node, macparams):
        """
        MAC constructor
        @node     : Node
        @cwmin    : Minimum contention window
        @m        : Maximum #of back off stages
        @rlimit   : Retry Limit
        """
        Process.__init__(self, name="mprdcf" + str(node.id), sim=node.sim)
        self.node = node
        self.cwmin = macparams["cwmin"]
        self.rmax = macparams["rlimit"]
        self.m = macparams["m"]
        self.cwmax = (2 ** self.m) * self.cwmin  # Max Contention window size
        self.macdelays = []

    class _IdleDIFSk(SimEvent):
        """
        IdleDIFS event: Generated when the channel is idle for DIFS
        duration. This is an MPR difs event as opposed to classical difs.
        """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, sim=node.channel.sim)

            class __IdleDIFSkProc(Process):
                """ idle difs event process """
                def idledifsk_delay(self, event):
                    """ wait until a DIFS delay elapses without ever reaching K
                    transmissions on the channel
                    """
                    channel = node.channel
                    while True:
                        while(channel.ntx >= channel.K):
                            yield (waitevent, self,
                            channel.uplink.Ch_Idle_EventK)
                        assert(channel.ntx < channel.K)
                        TO = TimeOut(delay=channel.difs, sim=self.sim)
                        while True:
                            """ wait till DIFS duration elapses before
                            channel go to busy state"""
                            # We don't wait for the tx end events
                            yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                            if self.eventsFired[0] != TO:
                                if(channel.ntx >= channel.K):
                                    # reset the TO event-> wait for another TO
                                    break
                                # continue to wait for TO
                            else:
                                if(channel.ntx < channel.K):
                                    event.signal()
                                    return
                                # Wait for next TO
                                break

            temp = __IdleDIFSkProc(name="_IdleDIFSkProc" + str(node.id),
                                   sim=node.channel.sim)
            self.sim.activate(temp, temp.idledifsk_delay(self))

    class _IdleSlotk(SimEvent):
        """IdleSlot event """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, sim=node.channel.sim)

            class __IdleSlotkProc(Process):
                """ idle slot event process """
                def idleslotk_delay(self, event):
                    """ wait for delay """
                    channel = node.channel
                    while True:
                        while(channel.ntx >= channel.K):
                            yield (waitevent, self,
                                   channel.uplink.Ch_Idle_EventK)
                        TO = TimeOut(delay=channel.slot, sim=self.sim)
                        while True:
                            yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                            if self.eventsFired[0] != TO:
                                if(channel.ntx >= channel.K):
                                    # reset the TO event-> wait for another TO
                                    break
                                # continue to wait for TO
                            else:
                                if(channel.ntx < channel.K):
                                    event.signal()
                                    return
                                else:
                                    # Wait for next TO
                                    break

            temp = __IdleSlotkProc(name="__IdleSlotkProc" + str(node.id),
                                   sim=node.channel.sim)
            self.sim.activate(temp, temp.idleslotk_delay(self))

    class _Transmit(SimEvent, Process):
        """ Transmit a packet """

        def __init__(self, node):
            """ setup as event and process """
            SimEvent.__init__(self, name="__Transmit" + str(node.id),
                              sim=node.sim)
            Process.__init__(self, name="__Transmit" + str(node.id),
                             sim=node.sim)
            self.sim.activate(self, self.__transmit(self, node))

        def __transmit(self, event, node):
            """ transmit a packet"""
            yield request, self, node.channel.mutex, 1
            yield request, self, node.channel.subchannel
            node.channel.uplink.Tx_Start_Event.signal()
            yield release, self, node.channel.mutex, 1
            # Check for immediate collision
            if (node.channel.ntx > node.channel.K):
                yield hold, self, node.pkttxtime
                yield request, self, node.channel.mutex, 2
                node.channel.uplink.Tx_End_Event.signal()
                yield release, self, node.channel.subchannel
                yield release, self, node.channel.mutex, 2
                node.pktcollided = node.pktcollided + 1
                # pkttosend should not get decremented
                # node.pkttosend = node.pkttosend - 1
                node.pktsent = node.pktsent + 1
                event.signal(False)
                return

            TO = TimeOut(node.pkttxtime, self.sim)
            Collided = False
            while True:
                # See if any other node transmits during the time. If the
                # number of transmissions is larger than K, all packets should
                # be marked as collided
                yield waitevent, self, (node.channel.uplink.Tx_Start_Event, TO)
                if(self.eventsFired[0] == TO):
                    if not Collided:
                        node.pktsucc = node.pktsucc + 1
                        success = True
                    else:
                        node.pktcollided = node.pktcollided + 1
                        success = False
                    break
                else:
                    if (node.channel.ntx > node.channel.K):
                        Collided = True
            yield request, self, node.channel.mutex, 2
            yield release, self, node.channel.subchannel
            node.channel.uplink.Tx_End_Event.signal()
            yield release, self, node.channel.mutex, 2
            if(success):
                # Decrement if successful transmission
                node.pkttosend = node.pkttosend - 1
            node.pktsent = node.pktsent + 1
            event.signal(success)


class ieee802_11(dcf):
    """ Conventional ieee802.11 dcf """

    class __Backoff(SimEvent):
        """ backoff mechanism """

        def __init__(self, node, counter):
            """ Intitialize event """
            SimEvent.__init__(self, name="BackOff" + str(node.id),
                              sim=node.channel.sim)

            class __BackoffProcess(Process):
                """ back of process - no constructor"""

                def backoffprocess(self, counter, event):
                    """ actual BEB """
                    assert(node.channel.ntx == 0)
                    while(counter > 0):
                        TO = TimeOut(node.channel.slot, self.sim)
                        yield waitevent, self, (
                        node.channel.uplink.Tx_Start_Event, TO)
                        if self.eventsFired[0] != TO:
                            # If the channel becomes busy wait for
                            # another DIFS duration
                            yield waitevent, self, node.mac._IdleDIFS(node)
                        else:
                            # Decrement counter when a slot duration is
                            # elapsed with no transmissions
                            assert (node.channel.ntx == 0)
                            counter = counter - 1
                    event.signal()
            bop = __BackoffProcess(name="__BackOffProcess" + str(node.id) +
            ":counter=" + str(counter), sim=self.sim)
            self.sim.activate(bop, bop.backoffprocess(counter, self))

    def mac(self):
        """ CSMA main loop
        MAC algorithm as described in Bianchi's Paper
        IEEE802.11 DCF
        """
        while True:
            if(self.node.pkttosend <= 0 and (not self.node.saturated)):
                yield hold, self, self.node.channel.slot
                self.node.idleslots = self.node.idleslots + 1
            else:
                entryTime = self.sim.now()
                if self.node.new:  # A new node needs to be handled differently
                    self.node.new = False
                    if not self.node.channel.isBusy():
                        start = self.sim.now()
                        ID = self._IdleDIFS(self.node)
                        """ wait for channel to be idle(<K txn)
                        for DIFS duration """
                        yield waitevent, self, ID
                        """ If the channel was Idle for DIFS duration,
                        transmit the packet-> set counter=0
                        otherwise generate backoff unformly within 2*cwmin """
                        if ((self.sim.now() - start - self.node.channel.difs) <
                        self.node.channel.slot / 2):
                            counter = 0
                            CW = self.cwmin
                        else:
                            counter = int(random.uniform(0, 2 * self.cwmin))
                            CW = 2 * self.cwmin
                    else:
                        ID = self._IdleDIFS(self.node)
                        yield waitevent, self, ID
                        counter = int(random.uniform(0, 2 * self.cwmin))
                        CW = 2 * self.cwmin
                else:
                    ID = self._IdleDIFS(self.node)
                    yield waitevent, self, ID
                    counter = int(random.uniform(0, self.cwmin))
                    CW = self.cwmin
                r = 0  # retry counter
                while(True):
                    BO = self.__Backoff(self.node, counter)
                    yield waitevent, self, BO
                    """ After backoff transmit the packet """
                    # Removed the wait
                    #yield waitevent, self, self._IdleSlot(self.node)
                    Tr = self._Transmit(self.node)
                    yield waitevent, self, Tr
                    if Tr.signalparam or (CW == self.cwmax and r == self.rmax):
                        self.node.macdelays.append(self.sim.now() - entryTime)
                        if not Tr.signalparam and r == self.rmax:
                            self.node.pktdrop = self.node.pktdrop + 1
                            self.node.pkttosend = self.node.pkttosend - 1
                        break
                    if CW != self.cwmax:
                        CW = 2 * CW
                    else:
                        r = r + 1
                    counter = int(random.uniform(0, CW))
                    # Wait for the channel to be idle for DIFS duration
                    ID = self._IdleDIFS(self.node)
                    yield waitevent, self, ID


class async_ieee802_11(mprdcf):
    """ MAC per node"""

    class __Backoff(SimEvent):
        """ backoff mechanism """

        def __init__(self, node, counter):
            """ Intitialize event """
            SimEvent.__init__(self, name="BackOff" + str(node.id),
                              sim=node.channel.sim)

            class __BackoffProcess(Process):
                """ back of process - no constructor
                """

                def backoffprocess(self, counter, event):
                    """ actual BEB """
                    channel = node.channel
                    assert(channel.ntx < channel.K)
                    TO = TimeOut(channel.slot, self.sim)
                    while(counter > 0):
                        yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                        if self.eventsFired[0] != TO:
                            if(channel.ntx >= channel.K):
                                yield (waitevent, self,
                                           node.mac._IdleDIFSk(node))
                            else:
                                # Continue to wait for the end of slot
                                continue
                        else:
                            assert(channel.ntx < channel.K)
                            counter = counter - 1
                        TO = TimeOut(channel.slot, self.sim)
                    event.signal()

            bop = __BackoffProcess(name="__BackOffProcess" + str(node.id)
            + "counter-" + str(counter), sim=self.sim)
            self.sim.activate(bop, bop.backoffprocess(counter, self))

    def mac(self):
        """ csma main loop"""
        while True:
            if(self.node.pkttosend <= 0 and (not self.node.saturated)):
                yield hold, self, self.node.channel.slot
                self.node.idleslots = self.node.idleslots + 1
            else:
                entryTime = self.sim.now()
                if self.node.new:
                    self.node.new = False
                    if not self.node.channel.isBusyK():
                        start = self.sim.now()
                        ID = self._IdleDIFSk(self.node)
                        yield waitevent, self, ID
                        if ((self.sim.now() - start - self.node.channel.difs) <
                        self.node.channel.slot / 2):
                            counter = 0
                            CW = self.cwmin
                        else:
                            counter = int(random.uniform(0, 2 * self.cwmin))
                            CW = 2 * self.cwmin
                    else:
                        ID = self._IdleDIFSk(self.node)
                        yield waitevent, self, ID
                        counter = int(random.uniform(0, 2 * self.cwmin))
                        CW = 2 * self.cwmin
                else:
                    ID = self._IdleDIFSk(self.node)
                    yield waitevent, self, ID
                    counter = int(random.uniform(0, self.cwmin))
                    CW = self.cwmin
                r = 0
                while(True):
                    BO = self.__Backoff(self.node, counter)
                    yield waitevent, self, BO
                    # After backoff, transmit
                    # yield waitevent, self, self._IdleSlotk(self.node)
                    Tr = self._Transmit(self.node)
                    yield waitevent, self, Tr
                    #if CW == self.cwmax:
                    #    print('max')
                    if Tr.signalparam or (CW == self.cwmax and r == self.rmax):
                        self.node.macdelays.append(self.sim.now() - entryTime)
                        if not Tr.signalparam and r == self.rmax:
                            self.node.pktdrop = self.node.pktdrop + 1
                            self.node.pkttosend = self.node.pkttosend - 1
                        break
                    if CW != self.cwmax:
                        CW = 2 * CW
                    else:
                        r = r + 1
                    counter = int(random.uniform(0, CW))
                    ID = self._IdleDIFSk(self.node)
                    yield waitevent, self, ID


class async_proposed(dcf):
    """ MAC per node """

    class __Backoff(SimEvent):
        """ backoff mechanism """

        def __init__(self, node, counter):
            """ Intitialize event """
            SimEvent.__init__(self, name="BackOff" + str(node.id),
                              sim=node.channel.sim)

            class __BackoffProcess(Process):
                """ back of process - no constructor"""

                def backoffprocess(self, counter, event):
                    """ actual BEB """
                    channel = node.channel
                    assert(channel.ntx < channel.K)
                    TO = TimeOut(channel.slot, self.sim)
                    while(counter > 0):
                        yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                        if self.eventsFired[0] != TO:
                            if(channel.ntx >= channel.K):
                                yield (waitevent, self,
                                           node.mac._IdleDIFS(node))
                            else:
                                # Continue to wait for the end of slot
                                continue
                        else:
                            assert(channel.ntx < channel.K)
                            counter = counter - (channel.K - channel.ntx)
                        TO = TimeOut(channel.slot, self.sim)
                    event.signal()
            bop = __BackoffProcess(name="BackOffProcess" + str(node.id),
                                   sim=self.sim)
            self.sim.activate(bop, bop.backoffprocess(counter, self))

    def mac(self):
        """ csma main loop"""
        while True:
            if(self.node.pkttosend <= 0 and (not self.node.saturated)):
                yield hold, self, self.node.channel.slot
                self.node.idleslots = self.node.idleslots + 1
            else:
                entryTime = self.sim.now()
                if self.node.new:
                    self.node.new = False
                    if not self.node.channel.isBusyK():
                        start = self.sim.now()
                        ID = self._IdleDIFS(self.node)
                        yield waitevent, self, ID
                        if ((self.sim.now() - start - self.node.channel.difs) <
                        self.node.channel.slot / 2):
                            counter = 0
                            CW = self.cwmin
                        else:
                            counter = int(random.uniform(0, 2 * self.cwmin))
                            CW = 2 * self.cwmin
                    else:
                        ID = self._IdleDIFS(self.node)
                        yield waitevent, self, ID
                        counter = int(random.uniform(0, 2 * self.cwmin))
                        CW = 2 * self.cwmin
                else:
                    ID = self._IdleDIFS(self.node)
                    yield waitevent, self, ID
                    counter = int(random.uniform(0, self.cwmin))
                    CW = self.cwmin
                r = 0
                while(True):
                    BO = self.__Backoff(self.node, counter)
                    yield waitevent, self, BO
                    # After backoff, transmit the packet
                    #yield waitevent, self, self._IdleSlotk(self.node)
                    Tr = self._Transmit(self.node)
                    yield waitevent, self, Tr
                    #if CW == self.cwmax:
                    #    print('max')
                    if Tr.signalparam or (CW == self.cwmax and r == self.rmax):
                        self.node.macdelays.append(self.sim.now() - entryTime)
                        if not Tr.signalparam and r == self.rmax:
                            self.node.pktdrop = self.node.pktdrop + 1
                            self.node.pkttosend = self.node.pkttosend - 1
                        break
                    if CW != self.cwmax:
                        CW = 2 * CW
                    else:
                        r = r + 1
                    counter = int(random.uniform(0, CW))
                    ID = self._IdleDIFS(self.node)
                    yield waitevent, self, ID


class async_proposed_t(mprdcf):
    """ Asynchronous proposed protocol(threshold)"""
    def __init__(self, node, macparams):
        mprdcf.__init__(self, node, macparams)
        # Threshold defaults to K-1
        try:
            self.threshold = macparams["threshold"]
        except:
            self.threshold = node.channel.K - 1

    class _IdleSlot(SimEvent):
        """IdleSlot event """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, sim=node.channel.sim)

            class __IdleSlotProc(Process):
                """ idle slot event process """
                def idleslot_delay(self, event):
                    """ wait for delay """
                    channel = node.channel
                    threshold = node.mac.threshold
                    while True:
                        while(channel.ntx > threshold):
                            yield (waitevent, self,
                                   channel.uplink.Tx_End_Event)
                        TO = TimeOut(delay=channel.slot, sim=self.sim)
                        while True:
                            yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                            if self.eventsFired[0] != TO:
                                if(channel.ntx > threshold):
                                    break
                            else:
                                if(channel.ntx > threshold):
                                    break
                                else:
                                    event.signal()
                                    return

            temp = __IdleSlotProc(name="__IdleSlotProc" + str(node.id),
                                  sim=node.channel.sim)
            self.sim.activate(temp, temp.idleslot_delay(self))

    class _IdleDIFSt(SimEvent):
        """IdleSlot event """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, sim=node.channel.sim)

            class __IdleDIFStProc(Process):
                """ idle slot event process """
                def idledifst_delay(self, event):
                    """ wait for delay """
                    channel = node.channel
                    threshold = node.mac.threshold
                    while True:
                        while(channel.ntx > threshold):
                            yield (waitevent, self,
                                   channel.uplink.Tx_End_Event)
                        TO = TimeOut(delay=channel.difs, sim=self.sim)
                        while True:
                            yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                            if self.eventsFired[0] != TO:
                                if(channel.ntx > threshold):
                                    break
                            else:
                                if(channel.ntx > threshold):
                                    break
                                else:
                                    event.signal()
                                    return

            temp = __IdleDIFStProc(name="__IdleDIFStProc" + str(node.id),
                                  sim=node.channel.sim)
            self.sim.activate(temp, temp.idledifst_delay(self))

    class __Backoff(SimEvent):
        """ backoff mechanism """

        def __init__(self, node, counter):
            """ Intitialize event """
            SimEvent.__init__(self, name="BackOff", sim=node.channel.sim)

            class __BackoffProcess(Process):
                """ back of process - no constructor"""

                def backoffprocess(self, counter, event):
                    """ actual BEB """
                    channel = node.channel
                    threshold = node.mac.threshold
                    assert(channel.ntx <= threshold)
                    TO = TimeOut(channel.slot, self.sim)
                    while(counter > 0):
                        yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                        if self.eventsFired[0] != TO:
                            if(channel.ntx > threshold):
                                yield (waitevent, self,
                                           node.mac._IdleDIFSt(node))
                            else:
                                # Continue to wait for the end of slot
                                continue
                        else:
                            if(channel.ntx <= threshold):
                                counter = counter - (channel.K - channel.ntx)
                        TO = TimeOut(channel.slot, self.sim)
                    event.signal()
            bop = __BackoffProcess(name="BackOffProcess" + str(node.id),
                                   sim=self.sim)
            self.sim.activate(bop, bop.backoffprocess(counter, self))

    def mac(self):
        """ csma main loop"""
        node = self.node
        channel = node.channel
        threshold = node.mac.threshold
        while True:
            if(self.node.pkttosend <= 0 and (not node.saturated)):
                yield hold, self, channel.slot
                node.idleslots = node.idleslots + 1
            else:
                entryTime = self.sim.now()
                if self.node.new:
                    self.node.new = False
                    if channel.ntx <= threshold:
                        start = self.sim.now()
                        ID = self._IdleDIFSt(self.node)
                        yield waitevent, self, ID
                        if ((self.sim.now() - start - self.node.channel.difs) <
                        self.node.channel.slot / 2):
                            counter = 0
                            CW = self.cwmin
                        else:
                            counter = int(random.uniform(0, 2 * self.cwmin))
                            CW = 2 * self.cwmin
                    else:
                        ID = self._IdleDIFSt(self.node)
                        yield waitevent, self, ID
                        counter = int(random.uniform(0, 2 * self.cwmin))
                        CW = 2 * self.cwmin
                else:
                    ID = self._IdleDIFSt(self.node)
                    yield waitevent, self, ID
                    counter = int(random.uniform(0, self.cwmin))
                    CW = self.cwmin
                r = 0
                while(True):
                    BO = self.__Backoff(self.node, counter)
                    yield waitevent, self, BO
                    # Once the backoff is complete, transmit the packet
                    #yield waitevent, self, self._IdleSlot(self.node)
                    Tr = self._Transmit(self.node)
                    yield waitevent, self, Tr
                    #if CW == self.cwmax:
                    #    print('max')
                    if Tr.signalparam or (CW == self.cwmax and r == self.rmax):
                        self.node.macdelays.append(self.sim.now() - entryTime)
                        if not Tr.signalparam and r == self.rmax:
                            self.node.pktdrop = self.node.pktdrop + 1
                            self.node.pkttosend = self.node.pkttosend - 1
                        break
                    if CW != self.cwmax:
                        CW = 2 * CW
                    else:
                        r = r + 1
                    counter = int(random.uniform(0, CW))
                    ID = self._IdleDIFSt(self.node)
                    yield waitevent, self, ID


class async_threshold_difs0(mprdcf, dcf):
    """ Here the protocol is asycnhronous but the DIFS requires the channel
    to be idle for DIFS duratiosn"""
    def __init__(self, node, macparams):
        mprdcf.__init__(self, node, macparams)
        # Threshold defaults to K-1
        try:
            self.threshold = macparams["threshold"]
        except:
            self.threshold = node.channel.K - 1

    class _IdleSlot(SimEvent):
        """IdleSlot event """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, sim=node.channel.sim)

            class __IdleSlotProc(Process):
                """ idle slot event process """
                def idleslot_delay(self, event):
                    """ wait for delay """
                    channel = node.channel
                    threshold = node.mac.threshold
                    while True:
                        while(channel.ntx > threshold):
                            yield (waitevent, self,
                                   channel.uplink.Tx_End_Event)
                        TO = TimeOut(delay=channel.slot, sim=self.sim)
                        while True:
                            yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                            if self.eventsFired[0] != TO:
                                if(channel.ntx > threshold):
                                    break
                            else:
                                if(channel.ntx > threshold):
                                    break
                                else:
                                    event.signal()
                                    return

            temp = __IdleSlotProc(name="__IdleSlotProc" + str(node.id),
                                  sim=node.channel.sim)
            self.sim.activate(temp, temp.idleslot_delay(self))

    class __Backoff(SimEvent):
        """ backoff mechanism """

        def __init__(self, node, counter):
            """ Intitialize event """
            SimEvent.__init__(self, name="BackOff", sim=node.channel.sim)

            class __BackoffProcess(Process):
                """ back of process - no constructor"""

                def backoffprocess(self, counter, event):
                    """ actual BEB """
                    channel = node.channel
                    threshold = node.mac.threshold
                    assert(channel.ntx < channel.K)
                    TO = TimeOut(channel.slot, self.sim)
                    while(counter > 0):
                        yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                        if self.eventsFired[0] != TO:
                            if(channel.ntx >= channel.K):
                                yield (waitevent, self,
                                           node.mac._IdleDIFS(node))
                            else:
                                # Continue to wait for the end of slot
                                continue
                        else:
                            assert(channel.ntx < channel.K)
                            if(channel.ntx <= threshold):
                                counter = counter - 1
                        TO = TimeOut(channel.slot, self.sim)
                    event.signal()
            bop = __BackoffProcess(name="BackOffProcess" + str(node.id),
                                   sim=self.sim)
            self.sim.activate(bop, bop.backoffprocess(counter, self))

    def mac(self):
        """ csma main loop"""
        node = self.node
        threshold = node.mac.threshold
        channel = node.channel
        while True:
            if(self.node.pkttosend <= 0 and (not self.node.saturated)):
                yield hold, self, self.node.channel.slot
                self.node.idleslots = self.node.idleslots + 1
            else:
                entryTime = self.sim.now()
                if self.node.new:
                    self.node.new = False
                    if channel.ntx <= threshold:
                        start = self.sim.now()
                        ID = self._IdleDIFS(self.node)
                        yield waitevent, self, ID
                        if ((self.sim.now() - start - self.node.channel.difs) <
                        self.node.channel.slot / 2):
                            counter = 0
                            CW = self.cwmin
                        else:
                            counter = int(random.uniform(0, 2 * self.cwmin))
                            CW = 2 * self.cwmin
                    else:
                        ID = self._IdleDIFS(self.node)
                        yield waitevent, self, ID
                        counter = int(random.uniform(0, 2 * self.cwmin))
                        CW = 2 * self.cwmin
                else:
                    ID = self._IdleDIFS(self.node)
                    yield waitevent, self, ID
                    counter = int(random.uniform(0, self.cwmin))
                    CW = self.cwmin
                r = 0
                while(True):
                    BO = self.__Backoff(self.node, counter)
                    yield waitevent, self, BO
                    #C Transmit the packet
                    #yield waitevent, self, self._IdleSlot(self.node)
                    Tr = self._Transmit(self.node)
                    yield waitevent, self, Tr
                    #if CW == self.cwmax:
                    #    print('max')
                    if Tr.signalparam or (CW == self.cwmax and r == self.rmax):
                        self.node.macdelays.append(self.sim.now() - entryTime)
                        if not Tr.signalparam and r == self.rmax:
                            self.node.pktdrop = self.node.pktdrop + 1
                            self.node.pkttosend = self.node.pkttosend - 1
                        break
                    if CW != self.cwmax:
                        CW = 2 * CW
                    else:
                        r = r + 1
                    counter = int(random.uniform(0, CW))
                    ID = self._IdleDIFS(self.node)
                    yield waitevent, self, ID


class async_threshold_difsk(mprdcf):
    """ Asynchronous threshold based protocol"""
    def __init__(self, node, macparams):
        mprdcf.__init__(self, node, macparams)
        # Threshold defaults to K-1
        try:
            self.threshold = macparams["threshold"]
        except:
            self.threshold = node.channel.K - 1
        try:
            self.decr = macparams["decrement"]
        except:
            self.decr = 1

    class _IdleSlot(SimEvent):
        """IdleSlot event """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, sim=node.channel.sim)

            class __IdleSlotProc(Process):
                """ idle slot event process """
                def idleslot_delay(self, event):
                    """ wait for delay """
                    channel = node.channel
                    threshold = node.mac.threshold
                    while True:
                        while(channel.ntx > threshold):
                            yield (waitevent, self,
                                   channel.uplink.Tx_End_Event)
                        TO = TimeOut(delay=channel.slot, sim=self.sim)
                        while True:
                            yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                            if self.eventsFired[0] != TO:
                                if(channel.ntx > threshold):
                                    break
                            else:
                                if(channel.ntx > threshold):
                                    break
                                else:
                                    event.signal()
                                    return

            temp = __IdleSlotProc(name="__IdleSlotProc" + str(node.id),
                                  sim=node.channel.sim)
            self.sim.activate(temp, temp.idleslot_delay(self))

    class _IdleDIFSt(SimEvent):
        """IdleSlot event """

        def __init__(self, node):
            """ Initialize with channel """
            SimEvent.__init__(self, sim=node.channel.sim)

            class __IdleDIFStProc(Process):
                """ idle slot event process """
                def idledifst_delay(self, event):
                    """ wait for delay """
                    channel = node.channel
                    threshold = node.mac.threshold
                    while True:
                        while(channel.ntx > threshold):
                            yield (waitevent, self,
                                   channel.uplink.Tx_End_Event)
                        TO = TimeOut(delay=channel.difs, sim=self.sim)
                        while True:
                            yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                            if self.eventsFired[0] != TO:
                                if(channel.ntx > threshold):
                                    break
                            else:
                                if(channel.ntx > threshold):
                                    break
                                else:
                                    event.signal()
                                    return

            temp = __IdleDIFStProc(name="__IdleDIFStProc" + str(node.id),
                                  sim=node.channel.sim)
            self.sim.activate(temp, temp.idledifst_delay(self))

    class __Backoff(SimEvent):
        """ backoff mechanism """

        def __init__(self, node, counter):
            """ Intitialize event """
            SimEvent.__init__(self, name="BackOff", sim=node.channel.sim)

            class __BackoffProcess(Process):
                """ back of process - no constructor"""

                def backoffprocess(self, counter, event):
                    """ actual BEB """
                    channel = node.channel
                    threshold = node.mac.threshold
                    decr = node.mac.decr
                    assert(channel.ntx <= threshold)
                    TO = TimeOut(channel.slot, self.sim)
                    while(counter > 0):
                        yield (waitevent, self,
                            (channel.uplink.Tx_Start_Event, TO))
                        if self.eventsFired[0] != TO:
                            if(channel.ntx > threshold):
                                yield (waitevent, self,
                                           node.mac._IdleDIFSt(node))
                            else:
                                # Continue to wait for the end of slot
                                continue
                        else:
                            assert(channel.ntx <= threshold)
                            if(channel.ntx <= threshold):
                                counter = counter - decr
                        TO = TimeOut(channel.slot, self.sim)
                    event.signal()
            bop = __BackoffProcess(name="BackOffProcess" + str(node.id),
                                   sim=self.sim)
            self.sim.activate(bop, bop.backoffprocess(counter, self))

    def mac(self):
        """ csma main loop"""
        node = self.node
        channel = node.channel
        threshold = node.mac.threshold
        while True:
            if(self.node.pkttosend <= 0 and (not node.saturated)):
                yield hold, self, channel.slot
                node.idleslots = node.idleslots + 1
            else:
                entryTime = self.sim.now()
                if self.node.new:
                    self.node.new = False
                    if channel.ntx <= threshold:
                        start = self.sim.now()
                        ID = self._IdleDIFSt(self.node)
                        yield waitevent, self, ID
                        if ((self.sim.now() - start - self.node.channel.difs) <
                        self.node.channel.slot / 2):
                            counter = 0
                            CW = self.cwmin
                        else:
                            counter = int(random.uniform(0, 2 * self.cwmin))
                            CW = 2 * self.cwmin
                    else:
                        ID = self._IdleDIFSt(self.node)
                        yield waitevent, self, ID
                        counter = int(random.uniform(0, 2 * self.cwmin))
                        CW = 2 * self.cwmin
                else:
                    ID = self._IdleDIFSt(self.node)
                    yield waitevent, self, ID
                    counter = int(random.uniform(0, self.cwmin))
                    CW = self.cwmin
                r = 0
                while(True):
                    BO = self.__Backoff(self.node, counter)
                    yield waitevent, self, BO
                    # Transmit the packet
                    #yield waitevent, self, self._IdleSlot(self.node)
                    Tr = self._Transmit(self.node)
                    yield waitevent, self, Tr
                    #if CW == self.cwmax:
                    #    print('max')
                    if Tr.signalparam or (CW == self.cwmax and r == self.rmax):
                        self.node.macdelays.append(self.sim.now() - entryTime)
                        if not Tr.signalparam and r == self.rmax:
                            self.node.pktdrop = self.node.pktdrop + 1
                            self.node.pkttosend = self.node.pkttosend - 1
                        break
                    if CW != self.cwmax:
                        CW = 2 * CW
                    else:
                        r = r + 1
                    counter = int(random.uniform(0, CW))
                    ID = self._IdleDIFSt(self.node)
                    yield waitevent, self, ID

#Results
#CSMA/CA based MAC Protocols
