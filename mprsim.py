#!/usr/bin/python3.2 -tt
# -*- coding: UTF-8 -*-
""" CSMA MPR simulation """

__version__ = "$Revision: 1.1 $"
# $Source$

__author__ = "Arun I B <arunib@smail.iitm.ac.in>"
__copyright__ = "Copyright (C) 2014 Arun I B"
#__license__     = TBD

from SimPy.Simulation import (Simulation, now)
import math
#from theory import *
import mac.csma
import application.poisson

# N nodes trying to contend for an MPR Channel of 1 Mbps capacity.
# Nodes asynchrounously try to access the channel and send a packet of
# constant length
#
# (pkLen_Kb) with packets being generated at each node in exponential
# inter arrival times with mean arrival time given
# by inter-arrival time_us

'''
Idealized CSMA/CA channel
-----------------------------
A node with a new packet to transmit monitors the channel activity

* All packets are of equal length.
* Packet transmission time is one full slot.
* If a node has packet to send, it will wait for DIFS duration after
a busy slot is over and transmit the packet.
* If more than one station transmits packets in the same slot,
there is a collision, and the receivers cannot receive the packets correctly.

* Successful transmission happens only when there is exactly one packet
transmission for the entire packet transmission time

* Assume that there is an immediate feedback from the receiver about the status
of each transmitted packet at the end of packet transmission time (0,1,e)
(i.e., correctly received or not). If no packet is transmitted in a slot,
 the slot is called idle.
* If its a success proceed to next packet.
* If collision, it will generate a random backoff and wait for that many
idle slots.

* We also assume that there is infinite buffering


This (0,1,e) feedback is modeled within our description of the csma channel.

In the case of a collision, the colliding packets are retransmitted
at a later slot after a randomly chosen backoff period.

Such packets are also called backlogged packets.

Parameters
Capacity        = 1Mbps
Slot Duration(Mini Slot) \sigma  = 50 micro secs
CWmin           = 16
CWmax           = 1024
Channel Bit Rate= 100 Mbit/s
ACK_Timeout     = 300 μs
MAC header      = 272 bits
PHY header      = 128 bits
SIFS=           = 28 micro seconds
DIFS            = 128 micro seconds

Packet Length = 8184 + 273 + 128

'''


class Node:
    """Lamda is the number of packets generated per slot time """
    def __init__(self, sim, NodeId, channel, applnparams, macparams):
        self.startTime = now()
        self.name = 'Node' + str(NodeId)
        self.id = NodeId
        self.sim = sim
        self.channel = channel
        self.saturated = applnparams["saturated"]
        self.pktcollided = 0  # number of collided packets
        self.pktsucc = 0  # count of packets successfully transmitted
        self.pktsent = 0  # count of packets transmitted (success or collision)
        self.idleslots = 0  # idle slots counter
        self.pkttosend = 0  # packets in buffer/tx queue
        self.pktdrop = 0  # total number of transmissions
        self.macdelays = []
        #self.pktLen_Kb = 1000 #0.064 #KiloBytes
        #self.pktLen = float(self.pktLen_Kb)*1000*8 #Bits
        self.pktlen = 8184 + 272 + 128  # FIXME move to app layer!!!
        self.pkttxtime = float(self.pktlen) / self.channel.C

        # Normalisezed lamda to normal
        if(not self.saturated):
            # Lambda is the rate of arrival of packets. The applnparams(lambda)
            # is normalized offered traffic. It needs to be multiplied by
            # channel capacity to get actual offered traffic in bits/s. To
            # convert this into packets/s, we need to divide it by the packet
            # length.
            self.Lambda = (applnparams["lambda"] *
            (float(self.channel.C) / self.pktlen))
            # Mean inter-arrival time
            self.arrivalTime = (float(1) / self.Lambda)
            self.application = application.poisson.Application(self)
        # Create a mac with the received parameters
        # Associate that mac protocol with the given node
        self.mac = macparams["protocol"](self, macparams=macparams)
        # A new node is handled specially in the dcf protocol
        self.new = True
        self.node()
        return

    def node(self):
        """ If unsaturated, the packet arrival process is started """
        if(not self.saturated):
            self.sim.activate(self.application, self.application.application())
        self.sim.activate(self.mac, self.mac.mac())
        return

    def throughput(self):
        """ Throughput is calculated as the time spent on successful
        transmissions(by this node) which is obtained as # of successful
        transmissions times the time it takes for a single transmission.
        This works even for MPR too. Note that this method will not work when
        the packet lengths are not constant.
        """
        return (float(self.pktsucc) * self.pkttxtime)

    def avgmacdelay(self):
        """ Calculate the mean of the channel access delays """
        if(len(self.macdelays) != 0):
            return (sum(self.macdelays) / len(self.macdelays))
        else:
            return 0

    def powefficiency(self):
        """ Number of transmission attempts per successfule packets """
        if(self.pktsucc != 0):
            return (float(self.pktsent) / self.pktsucc)
        else:
            return 0


class MPRModel(Simulation):
    """ MPR Simulation """

    def __init__(self, nNodes, applnparams, channelparams, macparams):
        """ Initialize parameter values
        * Lambda is not initilized here as it needs channel capacity
        information which is not available at this point
        """
        Simulation.__init__(self)
        self.nNodes = nNodes
        self.saturated = applnparams["saturated"]
        self.BitRate = channelparams["bitrate"]
        self.MPR = channelparams["mpr"]
        self.applnparams = applnparams
        self.channelparams = channelparams
        self.macparams = macparams
        self.attemptRate = 0
        self.throughput = 0
        self.macdelay = 0

    def runModel(self):
        """ Model routine """
        self.initialize()
        maxTime = math.pow(10, 1)  # FIXME Move to caller
        self.maxTime = maxTime
        c = self.channelparams["channel"](sim=self,
        channelparams=self.channelparams)
        macparams = self.macparams
        applnparams = self.applnparams
        nodeList = []
        for i in range(self.nNodes):
            nodeList.append(Node(sim=self, NodeId=i, channel=c,
                applnparams=applnparams, macparams=macparams))
        self.simulate(until=maxTime)
        tp = []
        delay = []
        efficiency = []
        # node.throughput() returns the time spent by each node on successful
        # transmissions. To normalize this, it needs to be divided by the total
        # system time. The aggregate throughput is found by adding the
        # throughputs of individual nodes. In the case of delay, its aggregate
        # delay does not make much sense, so we calculate the mean delay !
        for i in range(len(nodeList)):
            n = nodeList[i]
            tp.append(n.throughput() / maxTime)
            delay.append(n.avgmacdelay())
            efficiency.append(n.powefficiency())
        self.throughput = float(sum(tp))  # / len(tp)
        self.macdelay = float(sum(delay)) / len(delay)
        self.powefficiency = sum(efficiency)/len(efficiency)


def frange(start, stop, step):
    """ FIXME move to common tools range for floats """
    x = start
    while True:
        if x >= stop:
            return
        yield x
        x += step
